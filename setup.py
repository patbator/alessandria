import re
import os
from setuptools import setup, find_packages
from pip.req import parse_requirements


def read(*parts):
    return open(os.path.join(os.path.dirname(__file__), *parts)).read()

def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

def get_package_data(package):
    """
    Return all files under the root package, that are not in a
    package themselves.
    """
    dir_to_exclude = ['__pycache__',]
    file_ext_to_exclude = ['.pyc', '.log',]
    file_pref_to_exclude = ['temp',]
    walk = [(dirpath.replace(package + os.sep, '', 1), filenames) # Remove 1st occurence of 'alessandria/' in dirpath
            for dirpath, dirnames, filenames in os.walk(package)
            if not os.path.exists(os.path.join(dirpath, '__init__.py'))]

    filepaths = []
    for base, filenames in walk:
        for d in dir_to_exclude:
            if base.find(d) >= 0:
                print(base, d)
                break
        else:
            for filename in filenames:
                if (os.path.splitext(filename)[1] not in file_ext_to_exclude and 
                    os.path.splitext(filename)[0] not in file_pref_to_exclude):
                    filepaths.append(os.path.join(base, filename))
    return {package: filepaths}

install_reqs = parse_requirements('alessandria/requirements.txt', session='hack')
reqs = [str(ir.req) for ir in install_reqs]

setup(
    name='django-alessandria',
    version=find_version('alessandria', '__init__.py'),
    description='Book library application for Django',
    author='Marc Schneider',
    author_email='marc@mirelsol.org',
    license="GPL v3",
    long_description="See README.rd",
    keywords="book library django application",
    url='https://gitlab.com/mirelsol/alessandria',
    packages=find_packages(),
    zip_safe=False,
    package_data=get_package_data('alessandria'),
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Framework :: Django',
    ],
    install_requires=reqs
)
